<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="org.wejden.model.Commune" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Commune</title>
</head>
<body>
<h1>Commune</h1>
<% Commune commune =(Commune) request.getAttribute("commune"); %>
<p>ID = <%= commune.getId() %></p>
<p>Name = <%= commune.getName() %></p>

</body>
</html>