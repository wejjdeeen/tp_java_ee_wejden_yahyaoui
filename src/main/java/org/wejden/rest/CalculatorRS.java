package org.wejden.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("calcul")
public class CalculatorRS {
	
	@GET
	@Path("add/{x}/{y}")
	
	public int add(@PathParam("x") int a, @PathParam("y") int b ) {
		
		return a+b;
	}
	
	@GET
	@Path("bonjour/{a}")
	public String Bonjour (@PathParam("a") String annee) {
		return "Bonjour" + annee; 
	}
	

}
